import _ from './utils/_';
import colorTokens from './utils/color-tokens';
import getColors from './utils/get-colors';
import {
  getCombinationsAndPermutations,
  getMaxPermutations
} from './utils/helpers';
import svg from './utils/svg';
import {
  addDotControl,
  removeElements
} from './utils/view';

var buttonAdd = document.getElementById('button-add');
var buttonGenerate = document.getElementById('button-generate');
var toggleSwitch = document.getElementById('toggle-switch');
var svgTemplate;

var getSvgTemplate = setInterval(function () {
  svgTemplate = document.getElementById('svg-template');

  if (!svgTemplate) {
    return;
  }

  clearInterval(getSvgTemplate);
  init();
}, 200);

buttonAdd.onclick = addDotControl;
buttonGenerate.onclick = runSequence;
toggleSwitch.onclick = svg.setBackgroundColor;

function init() {
  var colorField = document.getElementsByClassName('color')[0];

  colorField.value = '#dddddd';

  svg.renderTemplate(svgTemplate);
  svg.setBackgroundColor();
}

function runSequence() {
  var colorFields = document.getElementsByClassName('color');
  var colors = getColors(colorFields);
  var uniqueColors = _.unique(colors);
  var cy = 0;
  var circle;
  var circles;
  var colorTokensList;
  var colorTokensMap;
  var cps;
  var lastCircle;
  var sortedCps;
  var viewBoxHeight;

  // resetSvg();
  removeElements('paper');
  svg.renderTemplate(svgTemplate);
  svg.setBackgroundColor.call(toggleSwitch);

  colorTokens.clear();
  colorTokens.set(uniqueColors);

  colorTokensList = colorTokens.getList();
  colorTokensMap = colorTokens.getMap();
  cps = getCombinationsAndPermutations(colorTokensList);
  sortedCps = cps.sort().reverse();

  console.log(getMaxPermutations(colorTokensList));

  // TODO: Add line numbers.
  // dotSvg();
  var svgElement = document.querySelector('svg');
  var i = sortedCps.length;

  while (i--) {
    var cpParts = sortedCps[i].split('').reverse();
    var cx = 0;
    var n = cpParts.length;

    cy += 15;

    while (n--) {
      circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
      cx += 15;

      circle.setAttribute('cx', cx);
      circle.setAttribute('cy', cy);
      circle.setAttribute('fill', '#' + colorTokensMap.get(cpParts[n]));
      circle.setAttribute('r', 5);

      svgElement.insertAdjacentElement('beforeend', circle);
    }
  }

  /*
  sortedCps.forEach(function (cp) {
    var cpParts = cp.split('');
    var cx = 0;

    cy += 15;

    cpParts.forEach(function (part) {
      circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
      cx += 15;

      circle.setAttribute('cx', cx);
      circle.setAttribute('cy', cy);
      circle.setAttribute('fill', '#' + map.get(part));
      circle.setAttribute('r', 5);

      svgElement.insertAdjacentElement('beforeend', circle);
    });
  });
  */

  circles = document.getElementsByTagName('circle');
  lastCircle = circles[circles.length - 1];
  viewBoxHeight = parseInt(lastCircle.getAttribute('cy'), 10) + 15;

  svgElement.setAttribute('viewBox', '0 0 1000 ' + viewBoxHeight);
}
