import typeChecker from 'type-checker';

export default {
  // array
  unique: function (array) {
    var uniqueItems = {};

    array.forEach(function (item) {
      uniqueItems[item] = item;
    });

    return this.getValues(uniqueItems);
  },
  // array or array-like
  getLastItem: function (array) {
    if (typeChecker.isArray(array) || !!array.length) {
      return array[array.length - 1];
    }
  },
  // math
  factorial: function (integer) {
    var product = 1;

    integer = parseInt(integer, 10) + 1;

    while (integer-- && integer > 0) {
      product *= integer;
    }

    return product;
  },
  // object
  getValues: function (object) {
    return typeChecker.isFunction(Object.values)
      ? Object.values(object)
      : (function () {
        var keys = Object.keys(object);

        return keys.map(function (key) {
          return object[key];
        });
      })();
  }
};
