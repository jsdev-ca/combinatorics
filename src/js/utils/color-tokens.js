import { list, map } from '../constants/tokenized-colors';

export default {
  clear() {
    list.length = 0;
    map.clear();
  },
  getList() {
    return list;
  },
  getMap() {
    return map;
  },
  set(colors) {
    let token = '';

    colors.forEach((color, index) => {
      token = index === 0
        ? 'a'
        : String.fromCharCode(token.charCodeAt(0) + 1);

      list.push(token);
      map.set(token, color);
    });
  }
};
