export default function (colorFields) {
  return Array.prototype.map.call(colorFields, colorField => {
    return colorField.value.slice(1);
  });
}
