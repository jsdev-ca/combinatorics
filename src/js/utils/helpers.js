import _ from './_';

export const getCombinationsAndPermutations = function (colors) {
  var i = 0;
  var numColors = colors.length;
  var output = [].concat(colors);
  var numColorsUsed;

  (function _() {
    var numOutputItems = output.length;
    var j;

    for (; i < numOutputItems; i++) {
      for (j = 0; j < numColors; j++) {
        if (output[i].indexOf(colors[j]) < 0) {
          output.push(output[i] + colors[j]);
        }
      }
    }

    numColorsUsed = output[output.length - 1].split('').length;

    if (numColorsUsed < numColors) {
      i = numOutputItems;
      _();
    }
  })();

  return output;
};

export const getMaxPermutations = function (array) {
  return array
    .map(function (item, index) {
      return index + 1;
    })
    .reverse()
    .reduce(function (sum, num, index, array) {
      sum += index === 0
        ? _.factorial(num)
        : _.factorial(array.length) / _.factorial(num);

      return sum;
    }, 0);
};
