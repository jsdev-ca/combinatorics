const renderTemplate = template => {
  const templateContainer = document.createElement('div');

  templateContainer.innerHTML = template.innerHTML;

  const svgContainer = document.querySelector('.container-svg');
  const newSvg = templateContainer.children[0];

  svgContainer.insertAdjacentElement('afterbegin', newSvg);
  svgContainer.classList.add('is-svg-loaded');
};

const setBackgroundColor = function () {
  const svg = document.querySelector('svg');
  const backgroundColor = this && this.checked ? '#272822' : '#ffffff';

  svg.style.backgroundColor = backgroundColor;
};

export default {
  renderTemplate,
  setBackgroundColor
};
