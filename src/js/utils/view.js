import _ from './_';

const addDotControl = function () {
  var newDotControl = document.createElement('div');
  var dotControlTemplate = document.getElementById('dot-control-template');
  var dotControls = document.getElementsByClassName('dot-control');
  var lastDotControl = _.getLastItem(dotControls);
  var removeButtons;
  var lastRemoveButton;

  newDotControl.innerHTML = dotControlTemplate.innerHTML;
  newDotControl.setAttribute('class', 'dot-control');
  lastDotControl.insertAdjacentElement('afterend', newDotControl);

  removeButtons = document.getElementsByClassName('button-remove');
  lastRemoveButton = _.getLastItem(removeButtons);
  lastRemoveButton.onclick = removeDotControl;
};

const removeElements = function (className, shouldPreserveFirstElement) {
  var elements = document.getElementsByClassName(className);
  var i = elements.length;
  var delimiter = shouldPreserveFirstElement ? 0 : -1;

  if (i) {
    while (i-- && i > delimiter) {
      elements[i].remove();
    }
  }
};

const removeDotControl = function () {
  this.parentNode.remove();
};

export {
  addDotControl,
  removeElements
};
